"""
=========================================================
                       GenOCL.py
 Generate a USE OCL specification from a UML package
=========================================================

FILL THIS SECTION AS SHOWN BELOW AND LINES STARTING WITH ###
@author Michael HORAK <horak.90@gmail.com>
@author Valerio POSTIGLIONE <valerio.postiglione@gmail.com>
@group  G70

Current state of the generator
----------------------------------
FILL THIS SECTION 
Explain which UML constructs are supported, which ones are not.

	Currently are supported all UML constructs that are used in
    CyberResidence model. That are Class, Abstract Class,
    Assosiation Class, Composition, Association, Enumeration,
    and Inheritance. We did not implement attribute visibility,
    n-ary, association, Interface, and Aggregation. These elements
    was not implemented because they are not needed in order to
    create a useOCL model for CyberResidence.

What is good in your generator?

	The generator works correctly according to the model we developed
	in the previous practice work. Furthermore, the generator can
	explore not only a single class, but the whole package, through
	the method 'package2OCL'. The generator also creates a file with
    the useOCL model but also it prints it into the script view in
    the Modelio. The file location is in the users home forlder for
    Linux and (if uncommented) on disc C: for windows users.

What are the current limitations?

	We do not see any big limitation, but the fact that in order to
	build this up we got inspired by a defined example, we assume
	we could not consider totally all the possible OCL definitions.

Current state of the tests
--------------------------
FILL THIS SECTION 
Explain how did you test this generator.

	We decided to test the generator in the following way. The first draft
	tests were performed on the Sandbox program, in order to get familiar
	with the mechanism to implement. Then, we went forward to the codeGen
	and tested each functionality by comparing the generated text with the
	one we had in our OCL model. For the final test, we did the same starting
	from the package root. Finally we tested our project also with the
    UMLTestCases package. Each generated OCL code we tried also to compile
    with the useOCL tool.

Which test are working? 

	From the UMLTestCases package are working all tests except 2 that
    are mentioned in the following part.

Which are not?

	Tests:
    for attribute visibility - visibility is not implemented
    for N-ary association - not implemented



Observations
------------

	We found this exercise quite helpful. If it is true that OCL is a very
	specific language that we did not know before, on the other side Python
	is very direct and this combination made the practice interesting.

Additional observations could go there
"""

classes = []            #Store all classes from the model
enumerations = []       #Store all enumerations from the model
constraints = []        #Store all constraints from the model

#---------------------------------------------------------
#   Helpers on the source metamodel (UML metamodel)
#---------------------------------------------------------
# The functions below can be seen as extensions of the
# modelio metamodel. They define useful elements that 
# are missing in the current metamodel but that allow to
# explorer the UML metamodel with ease.
# These functions are independent from the particular 
# problem at hand and could be reused in other 
# transformations taken UML models as input.
#---------------------------------------------------------

# example
def isAssociationClass(element):
    if element.getLinkToAssociation():
        return True
    else:
        return False

    """ 
    Return True if and only if the element is an association 
    that have an associated class, or if this is a class that
    has a associated association. (see the Modelio metamodel
    for details)
    """
    
 
#---------------------------------------------------------
#   Application dependent helpers on the source metamodel
#---------------------------------------------------------
# The functions below are defined on the UML metamodel
# but they are defined in the context of the transformation
# from UML Class diagramm to USE OCL. There are not
# intended to be reusable. 
#--------------------------------------------------------- 

# example
def associationsInPackage(packageClasses):
    packageAssociations = []

    for c in packageClasses:
        for a in c.getOwnedEnd():
            association = a.getAssociation()
            if isinstance(association, Association):
                if not association.getLinkToClass():
                    packageAssociations.append(association)

    return list(set(packageAssociations))

    """
    Return the list of all associations that start or
    arrive to a class which is recursively contained in
    a package.
    """
    
#---------------------------------------------------------
#   Helpers for the target representation (text)
#---------------------------------------------------------
# The functions below aims to simplify the production of
# textual languages. They are independent from the 
# problem at hand and could be reused in other 
# transformation generating text as output.
#---------------------------------------------------------


# for instance a function to indent a multi line string if
# needed, or to wrap long lines after 80 characters, etc.

#---------------------------------------------------------
#           Transformation functions: UML2OCL
#---------------------------------------------------------
# The functions below transform each element of the
# UML metamodel into relevant elements in the OCL language.
# This is the core of the transformation. These functions
# are based on the helpers defined before. They can use
# print statement to produce the output sequentially.
# Another alternative is to produce the output in a
# string and output the result at the end.
#---------------------------------------------------------



# examples

def umlEnumeration2OCL(enumeration):
    temp = []
    for enum in enumeration:
        temp.append('enum %s{ \n' % (enum.getName()))
        values = enum.getValue()
        for val in values:
            temp.append('\t %s' % (val.getName()))
            if val != values[-1]:
                temp.append(', \n')
            else:
                temp.append('\n')
        temp.append('} \n\n')
    return ''.join(temp)

    """
    Generate USE OCL code for the enumeration
    """


def umlBasicType2OCL(basicType):
    attrType = ''

    if basicType == 'string':
        attrType = 'String'
    elif basicType == 'integer':
        attrType = 'Integer'
    elif basicType == 'float':
        attrType = 'Real'
    elif basicType == 'boolean':
        attrType = 'Boolean'
    else:
        attrType = basicType
    return attrType

    """
    Generate USE OCL basic type. Note that
    type conversions are required.
    """


def package2OCL(package):

    model = []
    result = ''
    fileName = 'UmlToOCL.use'
    #For windows
    #fileName = 'C:/UmlToOCL.use'
    packageName = package.getName()
    packageElements = package.getOwnedElement()

    model.append(separate(packageName))
    model.append('model %s \n\n' % (packageName))
    selectClassesAndEnums(packageElements)
    model.append(umlEnumeration2OCL(enumerations))
    model.append(writeClasses())
    associations = associationsInPackage(classes)
    model.append(umlAssociations2OCL(associations))
    model.append(constraints2OCL(constraints))
    result = ''.join(model)

    print result
    try:
        file = open(fileName, 'w')
        file.write(result.encode('utf-8'))

    except IOError as exception:
        print 'IOError Exception raised ' + str(exception)
    finally:
        file.close()

    """
    Generate a complete OCL specification for a given package.
    The inner package structure is ignored. That is, all
    elements useful for USE OCL (enumerations, classes, 
    associationClasses, associations and invariants) are looked
    recursively in the given package and output in the OCL
    specification. The possibly nested package structure that
    might exist is not reflected in the USE OCL specification
    as USE is not supporting the concept of package.
    """


#Methods defined by me ------------------------------------------------------

def constraints2OCL(constraintsList):
    temp = []
    temp.append('constraints \n\n')

    for constrain in constraintsList:
        temp.append(separate(constrain.getName()))
        notes = constrain.getDescriptor()
        summary = ''
        description = ''
        comment = ''

        for n in notes:
            nType = n.getModel().getName()
            if nType == 'summary':
                summary = n.getContent()
            elif nType == 'description':
                description = n.getContent()
            elif nType == 'comment':
                comment = n.getContent()

        temp.append('-- %s \n-- %s \n\n' % (summary, comment))
        temp.append('--.context %s inv %s: \n\n' % (constrain.getOwner().getName(), constrain.getName()))
        temp.append('-- %s \n' % (description))

    return ''.join(temp)


def separate(name):
    separator = '--------------------------------------------------------------------------------------------- \n--    %s\n--------------------------------------------------------------------------------------------- \n' % (name)
    return separator


def umlAssociations2OCL(associations):
    temp = []
    assocName = ''

    for a in associations:
        typeOfAssociation = ''
        for ends in a.getEnd():
            if str(ends.getAggregation()) == 'KindIsComposition':
                typeOfAssociation = 'composition'
                break
            elif str(ends.getAggregation()) == 'KindIsAggregation':
                typeOfAssociation = 'aggregation'
                break
            elif str(ends.getAggregation()) == 'KindIsAssociation':
                typeOfAssociation = 'association'

        if a.getName():
            assocName = a.getName()
        else:
            assocName = ' '

        temp.append('%s %s ' % (typeOfAssociation, assocName))
        temp.append(writeAssociationToOCL(a))
        temp.append('end \n\n')
    return ''.join(temp)


def writeAssociationToOCL(association):
    temp = []
    endsName = ''
    ends = association.getEnd()
    temp.append('between \n')
    for e in ends:
        multiMax = e.getMultiplicityMax()
        multiMin = e.getMultiplicityMin()
        multiplicity = ''

        if multiMax == multiMin or (multiMax == '*' and multiMin == '0'):
            multiplicity = '[%s]' % (multiMax)
        else:
            multiplicity = '[%s..%s]' % (multiMin, multiMax)

        try:
            endsName = e.getTarget().getName()
        except Exception:
            endsName = ' '

        temp.append('\t%s%s' %(endsName, multiplicity))

        if e.getName():
            temp.append(' role %s' % (e.getName()))
        temp.append('\n')

    return ''.join(temp)

def selectClassesAndEnums(selElement):
    for e in selElement:
        if isinstance(e, Class):
            classes.append(e)
            constraints.extend(e.getOwnedElement())
        elif isinstance(e, Enumeration):
            enumerations.append(e)
        else:
            selectClassesAndEnums(e.getOwnedElement())


def operations2OCL(operations):
    temp = []
    temp.append('operations \n')
    retType = ''

    for o in operations:
        parameters = ''
        try:
            retType = o.getReturn().getType().getName()
        except:
            retType = ' '
        try:
            temp.append('\t-- %s \n' % (o.getDescriptor()[0].getContent()))
            if o.getReturn().getMultiplicityMax() == '*':
                temp.append('\t%s() : Set(%s) = \n' % (o.getName(), retType))
            else:
                raise Exception('Not a Set')
        except:
            try:
                param = o.getIO()
                for par in param:
                    parameters += par.getName() + ' : ' + umlBasicType2OCL(par.getType().getName())
                    if par != param[-1]:
                        parameters += ', '
            except:
                parameters = ''

            if retType == ' ':
                temp.append('\t%s(%s) \n' % (o.getName(), parameters))
            else:
                temp.append('\t%s(%s) : %s\n' % (o.getName(), parameters, retType))

        for desc in o.getCompositionChildren():
            if isinstance(desc, TemplateParameter):
                temp.append('\t\t%s \n' % (desc.getDescriptor()[0].getContent()))

    return ''.join(temp)



def writeClasses():
    temp = []
    for c in classes:
        attributes = c.getOwnedAttribute()
        operations = c.getOwnedOperation()
        assoc = False
        if c.isIsAbstract():
            temp.append('abstract class ')
        elif isAssociationClass(c):
            assoc = True
            temp.append('associationclass ')
        else:
            temp.append('class ')
        temp.append(c.getName())

        if c.getParent():
            for parent in c.getParent():
                temp.append(' < %s' % (parent.getSuperType().getName()))

        temp.append('\n')

        if assoc:
            temp.append(writeAssociationToOCL(c.getLinkToAssociation().getAssociationPart()))

        if len(attributes)>0:
            temp.append('attributes \n')
            for a in attributes:
                attrType = umlBasicType2OCL(a.getType().getName())
                temp.append('\t%s : %s' % (a.getName(), attrType))
                if a.isIsDerived():
                    temp.append('   -- @derived')
                temp.append('\n')

        if len(operations)>0:
            temp.append(operations2OCL(operations))
        temp.append('end \n\n')

    return ''.join(temp)

#---------------------------------------------------------
#           User interface for the Transformation 
#---------------------------------------------------------
# The code below makes the link between the parameter(s)
# provided by the user or the environment and the 
# transformation functions above.
# It also produces the end result of the transformation.
# For instance the output can be written in a file or
# printed on the console.
#---------------------------------------------------------

# (1) computation of the 'package' parameter
# (2) call of package2OCL(package)
# (3) do something with the result



# Main

try:
    if len(selectedElements)==0:
        print 'Please select a package'
    else:
        for p in selectedElements:
            package2OCL(p)

except Exception as exception:
    print 'Exception raised ' + str(exception)
